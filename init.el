(defun hello-world()
  "Say hello world"
  (interactive)
  (message "Hello World"))

(org-babel-load-file "~/p/emacs/config.org")
